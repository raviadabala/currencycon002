import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  constructor(public http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }




  getCountries(){
    return this.http.get("http://data.fixer.io/api/symbols?access_key=829e40366119c76a88fa96d83e701a94");
  }

}
